import inspect
import os
import sys


currentdir = os.path.dirname(
    os.path.abspath(
        inspect.getfile(
            inspect.currentframe()
        )
    )
)
parrentdir = os.path.dirname(currentdir)
src_path = os.path.join(parrentdir, 'src')
sys.path.insert(0, src_path)


from functions import \
    from_2_to_10, \
    from_10_to_2, \
    from_2_to_8, \
    from_8_to_2, \
    from_8_to_16, \
    from_16_to_8


def test_from_2_to_10():
    """
    Тест функции from_2_to_10().
    """
    assert from_2_to_10('2', '1100110011') == 819
    assert from_2_to_10('2', '0101011100001101001010') == 1426250
    assert from_2_to_10('2', '00000') == 0
    assert from_2_to_10('2', '1') == 1


def test_from_10_to_2():
    """
    Тест функции from_10_to_2().
    """
    assert from_10_to_2('2', '4327521') == '10000100000100001100001'
    assert from_10_to_2('2', '707') == '1011000011'
    assert from_10_to_2('2', '00000') == '0'
    assert from_10_to_2('2', '1') == '1'


def test_from_2_to_8():
    """
    Тест функции from_2_to_8().
    """
    assert from_2_to_8('2', '8', '1100110011') == '1463'
    assert from_2_to_8('2', '8', '1101110') == '156'
    assert from_2_to_8('2', '8', '0000') == '0'
    assert from_2_to_8('2', '8', '1') == '1'


def test_from_8_to_2():
    """
    Тест функции from_8_to_2().
    """
    assert from_8_to_2('8', '2', '1463') == '1100110011'
    assert from_8_to_2('8', '2', '156') == '1101110'
    assert from_8_to_2('8', '2', '000000') == '0'
    assert from_8_to_2('8', '2', '1') == '1'


def test_from_8_to_16():
    """
    Тест функции from_8_to_16().
    """
    assert from_8_to_16('8', '16', '1463') == '333'
    assert from_8_to_16('8', '16', '156') == '6E'
    assert from_8_to_16('8', '16', '00000') == '0'
    assert from_8_to_16('8', '16', '1') == '1'


def test_from_16_to_8():
    """
    Тест функции from_16_to_8().
    """
    assert from_16_to_8('16', '8', '333') == '1463'
    assert from_16_to_8('16', '8', '6E') == '156'
    assert from_16_to_8('16', '8', '00000') == '0'
    assert from_16_to_8('16', '8', '1') == '1'
